#ifndef SOLVE_ZERO_INPUT_HPP
#define SOLVE_ZERO_INPUT_HPP

#include "tools.hpp"
#include <sstream>
#include <iostream>

void solve_zero_input(const vector<double>& f1, const vector<double>& f2, vector<double> & y);

#endif