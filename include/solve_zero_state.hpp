#ifndef SOLVE_ZERO_STATE_HPP
#define SOLVE_ZERO_STATE_HPP

#include "tools.hpp"
#include <sstream>
#include <iostream>
#include <math.h>

void solve_zero_state(const double time_step, const double time_end, const matrix& A, const int order,
                      const vector<double>& y_0, const int prob_num);

void sample_data(const double& dt, const double& T, vector<double>& ft, vector<double>& ht);

#endif  // SOLVE_ZERO_STATE_HPP
