#ifndef TOOLS_HPP
#define TOOLS_HPP

#include <vector>
#include <cmath>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <string.h>

using namespace std;
using matrix = vector<vector<double>>;
void print_matrix(const matrix& A);
void mat_mul(const double& t, const matrix& A, matrix& B);
void mat_add(const matrix& A, const matrix& B, matrix& C);
void mat_sub(const matrix& A, const matrix& B, matrix& C);
double det_matrix(const matrix& A);
void vector_mat_mul(const vector<double>& v, const matrix& A, vector<double>& B);
void write_zero_state_sol(const matrix& A, const string header, const string file_name);
void init_square_matrix(matrix& A, const int order);
void write_zero_input_sol(const vector<double>& y, const string header, const string file_name);

#endif