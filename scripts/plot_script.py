import numpy as np
import glob
import matplotlib.pyplot as plt
import subprocess
import os
from pathlib import Path


# analytical solutions to differential equations


def extract_number(file_name):
    # Extract just the base file name (remove directory)
    base_name = os.path.basename(file_name)

    # Remove 'sol_' and '.csv' from the file name to get the number
    number = base_name.replace("sol_", "").replace(".csv", "")

    # Convert the extracted number to an integer
    return int(number)


def f_1(x):
    return 2 * np.exp(-3 * x)


def f_2(x):
    return np.exp(-0.2 * x) * (
        -298 / 4803 * np.cos(6 * x) + 8247 / 16010 * np.sin(6 * x)
    ) + (9904 / 4803) * np.exp(-0.05 * x)


def f_3(x):
    # Parameters
    R_1 = 1000
    R_2 = 47000
    C = 100e-6
    L = 5

    # Coefficients
    a = ((R_2 + R_1) / R_1) * L * C
    b = L / R_1 + R_2 * C
    c = 1

    # Solving roots
    coefficients = [a, b, c]
    rt = np.roots(coefficients)

    # Defining matrices
    a_matrix = np.array([5, 40.625])
    b_matrix = np.array([[1, 1], [rt[1], rt[0]]])

    # Solving the system using the pseudo-inverse
    sol = np.linalg.pinv(b_matrix).dot(a_matrix)

    # Output the solution
    print("Roots:", rt)
    print("Solution:", sol)

    return sol.item(0) * np.exp(-0.212771 * x) + sol.item(1) * np.exp(-195.829 * x)


if __name__ == "__main__":
    # get build directory information nad name of executable
    folder = Path(__file__).parent
    temp_path = "../build"
    working_directory = os.path.join(folder, temp_path)
    executable_name = "main"
    # run executable
    try:
        result = subprocess.run(
            [f"./{executable_name}"],
            cwd=working_directory,
            check=True,
            capture_output=True,
            text=True,
        )

        # print standard out
        print(result.stdout)
        # Print any errors that occurred during the execution and raise erro
        if result.stderr:
            print("Errors during execution:")
            print(result.stderr)
            raise RuntimeError
    # catch issues with running the executable
    except subprocess.CalledProcessError as e:
        print(f"An error occurred: {e}")
        raise e

    x = 0
    # get all csv files in the build folder
    file_names = glob.glob("build/sol_*.csv")

    # Sort the file names based on the extracted number
    file_names = sorted(file_names, key=extract_number)
    print(file_names)

    num_file_names = len(file_names)

    # pack the executables
    functions = [f_1, f_2, f_3]

    # create a subplot for display
    fig, ax = plt.subplots(num_file_names, 1)
    for i, file_name in enumerate(file_names):
        # extract data
        data = np.genfromtxt(fname=file_name, delimiter=", ", names=True)
        # get the names of the columns
        names = data.dtype.names
        # get the function values
        values = functions[i](data[names[-1]])
        # save image of just single problem
        f_single, ax_single = plt.subplots()
        ax_single.set_title(f"Problem #: {i}")
        ax_single.plot(data[names[-1]], data["x"], label="numerical")
        ax_single.plot(data[names[-1]], values, label="analytical")
        ax_single.set_xlabel("time")
        ax_single.legend()
        f_single.savefig(f"./build/problem_{i}.png")
        plt.close(f_single)

        # add to subplot for display
        if num_file_names != 1:
            ax[i].set_title(f"Problem #: {i}, ", pad=10)
            ax[i].plot(data[names[-1]], data["x"], label="numerical")
            ax[i].plot(data[names[-1]], values, label="analytical")
            ax[i].legend()
            ax[i].set_xlabel("time", labelpad=5)
        else:
            ax.set_title(f"Problem #: {i}, ", pad=10)
            ax.plot(data[names[-1]], data["x"], label="numerical")
            ax.plot(data[names[-1]], values, label="analytical")
            ax.legend()
            ax.set_xlabel("time", labelpad=5)
            
    plt.tight_layout()
    plt.legend()
    plt.show()
