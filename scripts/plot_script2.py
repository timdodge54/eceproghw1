import numpy as np
import glob
import matplotlib.pyplot as plt
import subprocess
import os
from pathlib import Path


# analytical solutions to differential equations
# Function to extract the letter from the filename

def get_letter_from_filename(filename):
    basename = os.path.basename(filename)
    name, _ = os.path.splitext(basename)
    # Extract the letter part (assuming it's the last character before the extension)
    letter = name[-1]
    return letter

def extract_number(file_name):
    # Extract just the base file name (remove directory)
    base_name = os.path.basename(file_name)

    # Remove 'sol_' and '.csv' from the file name to get the number
    number = base_name.replace("sol_", "").replace(".csv", "")

    # Convert the extracted number to an integer
    return int(number)


if __name__ == "__main__":
    # get build directory information nad name of executable
    folder = Path(__file__).parent
    temp_path = "../build"
    working_directory = os.path.join(folder, temp_path)
    executable_name = "main"
    # run executable
    try:
        result = subprocess.run(
            [f"./{executable_name}"],
            cwd=working_directory,
            check=True,
            capture_output=True,
            text=True,
        )

        # print standard out
        print(result.stdout)
        # Print any errors that occurred during the execution and raise erro
        if result.stderr:
            print("Errors during execution:")
            print(result.stderr)
            raise RuntimeError
    # catch issues with running the executable
    except subprocess.CalledProcessError as e:
        print(f"An error occurred: {e}")
        raise e

    x = 0
    # get all csv files in the build folder
    file_names = glob.glob("build/sol_*.csv")

    # Sort the file names based on the extracted number
    file_names = sorted(file_names, key=extract_number)
    print(file_names)

    num_file_names = len(file_names)

    folder_path = 'build/conv'

    # Check if the folder exists
    if not os.path.exists(folder_path):
        # Create the folder
        os.makedirs(folder_path)

    folder_path1 = 'build/figures'

    # Check if the folder exists
    if not os.path.exists(folder_path1):
        # Create the folder
        os.makedirs(folder_path1)
    
    csv_files = glob.glob(os.path.join(folder_path, '*.csv'))

    csv_files_sorted = sorted(csv_files, key=get_letter_from_filename)
    # Define your sequences
    f1 = [0, 1, 2, 3, 2, 1]
    f2 = [-2, -2, -2, -2, -2, -2, -2]
    f3 = [1, -1, 1, -1]
    f4 = [0, 0, 0, -3, -3]
    fs = [f1, f2, f3, f4]

    # Define the indices for convolution pairs
    indices = [(0, 0), (0, 1), (0, 2), (1, 2), (0, 3)]

    # Loop through each CSV file and plot the CSV data against the computed convolution
    for i, csv_file in enumerate(csv_files_sorted):
        # Read the CSV data, skipping the header line
        data = np.genfromtxt(csv_file, delimiter=',', skip_header=1)
        
        # Retrieve the indices of sequences to convolve
        i1, i2 = indices[i]
        
        # Perform the convolution in Python
        conv_res = np.convolve(fs[i1], fs[i2], mode="full")

        # Plotting
        plt.figure()
        
        # Plot CSV data as a bar chart
        x_positions_csv = np.arange(len(data))
        plt.bar(x_positions_csv, data, width=0.8, label='C++ implemented Convolution', color='skyblue')

        # Plot the Python convolution result as a line plot
        x_positions_conv = np.arange(len(conv_res))
        plt.plot(x_positions_conv, conv_res, label='Library Convolution', color='orange', marker='o')
        
        # Use the filename (without extension) as the plot title
        basename = os.path.basename(csv_file)
        name, _ = os.path.splitext(basename)
        plt.title(f'Comparison for {name}')

        # Label the axes
        plt.xlabel('Index')
        plt.ylabel('Value')
        plt.legend()
        plt.grid(True)

        # Display the plot
        output_filename = os.path.join(folder_path1, f"{name}.png")
        plt.savefig(output_filename)
        plt.show()

        # Close the figure to free memory
        plt.close()

    folder_path1 = 'build/data'

    # Check if the folder exists
    if not os.path.exists(folder_path1):
        # Create the folder
        os.makedirs(folder_path1)
    
    csv_files = glob.glob(os.path.join(folder_path1, '*.csv'))

    for csv_file in csv_files:
        data = np.genfromtxt(csv_file, delimiter=',', skip_header=1)
        

        # Plotting
        plt.figure()
        
        # Plot CSV data as a bar chart
        x_positions_csv = np.arange(0, 10.001, .001)
        plt.plot(x_positions_csv, data, color='skyblue')
        # Use the filename (without extension) as the plot title
        basename = os.path.basename(csv_file)
        name, _ = os.path.splitext(basename)
        plt.title(f'Points for {name}')
        plt.xlabel('Time')
        plt.ylabel('Value')
        plt.grid(True)

        # Display the plot
        output_filename = os.path.join(folder_path1, f"{name}.png")
        plt.savefig(output_filename)
        plt.show()

    folder_path1 = 'build/prob3'

    # Check if the folder exists
    if not os.path.exists(folder_path1):
        # Create the folder
        os.makedirs(folder_path1)
    
    csv_files = glob.glob(os.path.join(folder_path1, 'Problem*.csv'))
    
    for csv_file in csv_files:
        data = np.genfromtxt(csv_file, delimiter=',', skip_header=1)
        
        # Plotting
        plt.figure()
        
        # Plot CSV data as a bar chart
        x_positions_csv = np.arange(len(data)) *1.0
        x_positions_csv /= np.max(x_positions_csv)
        x_positions_csv *= 20
        plt.plot(x_positions_csv, data, color='skyblue')
        # Use the filename (without extension) as the plot title
        basename = os.path.basename(csv_file)
        name, _ = os.path.splitext(basename)
        plt.title(f'Convolution for f(t) * h(t) {name}')
        plt.xlabel('Time')
        plt.ylabel('Value')
        plt.grid(True)

        # Display the plot
        output_filename = os.path.join(folder_path1, f"{name}.png")
        plt.savefig(output_filename)
        plt.show()
    
    csv_files = glob.glob(os.path.join(folder_path1, 'sol_*.csv'))
        

    for csv_file in csv_files:
        data1 = np.genfromtxt(csv_file, delimiter=',', skip_header=1)
        
        # Plotting
        plt.figure()
        
        # Plot CSV data as a bar chart
        x_positions_csv = data1[:,1]
        plt.plot(x_positions_csv, data1[:,0], color='skyblue')
        # Use the filename (without extension) as the plot title
        basename = os.path.basename(csv_file)
        name, _ = os.path.splitext(basename)
        plt.title(f'Zero Input reponse {name}')
        plt.xlabel('Time')
        plt.ylabel('Value')
        plt.grid(True)

        # Display the plot
        output_filename = os.path.join(folder_path1, f"{name}.png")
        plt.savefig(output_filename)
        plt.show()

        plt.figure()

        total_response = data[1:]+data1[:, 0]
        plt.plot(x_positions_csv, total_response, color='skyblue')
        plt.title(f"Total System Response")
        plt.xlabel('Time')
        plt.ylabel("Value")
        plt.grid()
        output_filename = os.path.join(folder_path1, f"total_sys.png")
        plt.savefig(output_filename)
        plt.show()

        
