#include "solve_zero_state.hpp"
#include "solve_zero_input.hpp"

int main(int argc, char* argv[])
{
  const vector<double> f1 = { 0, 1, 2, 3, 2, 1 };
  const vector<double> f2 = { -2, -2, -2, -2, -2, -2, -2 };
  const vector<double> f3 = { 1, -1, 1, -1 };
  const vector<double> f4 = { 0, 0, 0, -3, -3 };
  vector<double> y;
  solve_zero_input(f1, f1, y);
  const string header_zero_state = "y";
  const string filename1 = "./conv/1a.csv";
  write_zero_input_sol(y, header_zero_state, filename1);
  solve_zero_input(f1, f2, y);
  const string filename2 = "./conv/1b.csv";
  write_zero_input_sol(y, header_zero_state, filename2);
  solve_zero_input(f1, f3, y);
  const string filename3 = "./conv/1c.csv";
  write_zero_input_sol(y, header_zero_state, filename3);
  solve_zero_input(f2, f3, y);
  const string filename4 = "./conv/1d.csv";
  write_zero_input_sol(y, header_zero_state, filename4);
  solve_zero_input(f1, f4, y);
  const string filename5 = "./conv/1e.csv";
  write_zero_input_sol(y, header_zero_state, filename5);
  const double dt = .001;
  const double T = 10;
  vector<double> ft;
  vector<double> ht;
  sample_data(dt, T, ft, ht);
  solve_zero_input(ft, ht, y);
  const string filename6 = "./prob3/Problem3.csv";
  write_zero_input_sol(y, header_zero_state, filename6);
  const string filename7 = "./data/ft.csv";
  write_zero_input_sol(ft, header_zero_state, filename7);
  const string filename8 = "./data/ht.csv";
  write_zero_input_sol(ht, header_zero_state, filename8);
  const double final_time = 20;
  const matrix A1 = { { 0, 1, 0 }, { 0, 0, 1 }, { -3, -38.25, -72.5 } };
  const vector<double> y0 = { -3, 2, 4 };
  const int order = 3;
  const int prob_3 = 3;
  solve_zero_state(dt, final_time, A1, order, y0, 3);

  return 0;
}
