#include "solve_zero_input.hpp"

void solve_zero_input(const vector<double>& f1, const vector<double>& f2, vector<double>& y)
{
  int leny = f1.size() + f2.size() - 1;  // Length of the resulting convolution array
  y.clear();
  for (int i = 0; i < leny; ++i)
  {
    y.push_back(0);
  }
  for (int n = 0; n < leny; n++)
  {
    // Set up a loop for m that remains within bounds for both f1 and f2
    for (int m = 0; m <= n; m++)
    {
      // Ensure m is a valid index for f1 and (n - m) is a valid index for f2
      if (m < f1.size() && (n - m) < f2.size())
      {
        y[n] += f1[m] * f2[n - m];
      }
    }
  }
}

void sample_data(const double& dt, const double& T, vector<double>& ft, vector<double>& ht)
{
  double current_time = 0.0;
  while (current_time <= T)
  {
    double sampled_value_ht =
        -2 / 149 * exp(-2 * current_time) + .1639 * exp(-.5 * current_time) * cos(6 * current_time - 1.491);
    double sampled_value_ft = sin(7 * M_PI * current_time);
    ft.push_back(sampled_value_ft);
    ht.push_back(sampled_value_ht);
    current_time += dt;
  }
}