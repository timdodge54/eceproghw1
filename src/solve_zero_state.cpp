#include "solve_zero_state.hpp"

void solve_zero_state(const double time_step, const double time_end, const matrix& A, const int order,
                      const vector<double>& y_0, const int prob_num)
{
  cout << "Problem " << prob_num << "..." << endl;
  int iter_count = 0;
  matrix I;
  init_square_matrix(I, order);
  for (int i = 0; i < order; ++i)
  {
    I[i][i] = 1.0;
  }

  matrix sol;

  vector<double> y = y_0;
  vector<double> temp = { y_0[0], iter_count * time_step };
  sol.push_back(temp);
  temp.clear();
  matrix A_t;
  matrix AI;
  init_square_matrix(A_t, A.size());
  mat_mul(time_step, A, A_t);
  init_square_matrix(AI, A.size());
  mat_add(I, A_t, AI);
  vector<double> zero_vec;
  for (int i = 0; i < order; ++i)
  {
    zero_vec.push_back(0.0);
  }
  vector<double> y_t = zero_vec;

  iter_count += 1;
  while (iter_count * time_step < time_end)
  {
    vector_mat_mul(y, AI, y_t);
    temp.push_back(y_t[0]);
    //  save results
    temp.push_back(iter_count * time_step);
    sol.push_back(temp);
    temp.clear();
    // clear matrices and increment
    y = y_t;
    y_t = zero_vec;
    if (iter_count % 200 == 0)
    {
      // cout << "i = " << iter_count << endl;
    }
    iter_count++;
  }

  cout << "Solution with time step " << time_step << " and " << iter_count << " iterations:" << endl;

  stringstream ss;
  ss << "./prob3/sol_" << prob_num << ".csv";
  const string header("x, time");
  const string file_name = ss.str();
  write_zero_state_sol(sol, header, file_name);
}