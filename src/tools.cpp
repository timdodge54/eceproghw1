#include "tools.hpp"

/*
    Prints a matrix to the console
    A = the matrix to print
*/
void print_matrix(const matrix& A)
{
  for (int i = 0; i < A.size(); i++)
  {
    for (int j = 0; j < A[0].size(); j++)
    {
      cout << A[i][j] << " ";
    }
    cout << endl;
  }
}
/*
    Multiplies matrix A by a scalar t
    A = the matrix to be multiplied
    t = the scalar to multiply by
    B
*/
void mat_mul(const double& t, const matrix& A, matrix& B)
{
  int i, j;
//   cout << "Mat Mul" << endl;
//   cout << "t = " << t << endl;
//   cout << "A = " << endl;
//   print_matrix(A);

  for (i = 0; i < A.size(); i++)
  {
    for (j = 0; j < A[0].size(); j++)
    {
      B[i][j] = t * A[i][j];
    }
  }
//   cout << "B = " << endl;
//   print_matrix(B);
}
/*
    Adds two matrices A and B
    A = the first matrix
    B = the second matrix
    C = the sum of A and B
*/
void mat_add(const matrix& A, const matrix& B, matrix& C)
{
//   cout << "Mat Add" << endl;
//   cout << "A = " << endl;
//   print_matrix(A);
//   cout << "B = " << endl;
//   print_matrix(B);
  for (int i = 0; i < A.size(); i++)
  {
    for (int j = 0; j < A[0].size(); j++)
    {
      C[i][j] = A[i][j] + B[i][j];
    }
  }
//   cout << "C = " << endl;
//   print_matrix(C);
}
/*
    subtracts two matrices A and B
    A = the first matrix
    B = the second matrix
    C = the sum of A and B
*/
void mat_sub(const matrix& A, const matrix& B, matrix& C)
{
//   cout << "Mat Add" << endl;
//   cout << "A = " << endl;
//   print_matrix(A);
//   cout << "B = " << endl;
//   print_matrix(B);
  for (int i = 0; i < A.size(); i++)
  {
    for (int j = 0; j < A[0].size(); j++)
    {
      C[i][j] = A[i][j] - B[i][j];
    }
  }
//   cout << "C = " << endl;
//   print_matrix(C);
}
/*
    Calculates the determinant of a matrix
    A = the matrix to calculate the determinant of
*/

double det_matrix(const matrix& A)
{
  double det = 0;
  if (A.size() == 1)
  {
    return A[0][0];
  }
  else if (A.size() == 2)
  {
    return A[0][0] * A[1][1] - A[0][1] * A[1][0];
  }
  matrix B;
  for (int i = 0; i < A[0].size(); i++)
  {
    vector<double> row;
    for (int j = 1; i < A.size(); j++)
    {
      if (j != i)
      {
        row.push_back(A[i][j]);
      }
    }
    if (row.size() > 0)
    {
      B.push_back(row);
    }
    det = det + A[0][i] * pow(-1, i) * det_matrix(B);
  }
  return det;
}


/*
    Multiplies a vector by a matrix
    v = the vector to be multiplied
    A = the matrix to be multiplied
    B = the product of v and A
*/
void vector_mat_mul(const vector<double>& v, const matrix& A, vector<double>& B)
{
  vector<double> temp(v.size(), 0.0);
//   cout << "Vector Mat Mul" << endl;
//   cout << "v = " << v.size() << endl;
//   for (size_t i = 0; i < v.size(); ++i)
//   {
//     cout << v[i] << " ";
//   }
//   cout << endl;
//   cout << "A = " << endl;
//   print_matrix(A);
  for (int i = 0; i < A.size(); i++)
  {
    for (int j = 0; j < A[0].size(); j++)
    {
      temp[i] += v[j] * A[i][j];
    }
  }
//   cout << "B = " << endl;
//   cout << B[0] << " " << B[1] << " " << B[2] << endl;
  B = temp;
}
/*
    Writes the context of the solution matrix A
*/
void write_zero_state_sol(const matrix& A, const string header, const string file_name)
{
  ofstream outfile(file_name);
  outfile << header << endl;
  for (int i = 0; i < A.size(); i++)
  {
    for (int j = 0; j < A[0].size(); j++)
    {
      if (j == A[0].size() - 1)
      {
        outfile << A[i][j];
      }
      else
      {
        outfile << A[i][j] << ", ";
      }
    }
    outfile << endl;
  }
  outfile.close();
}
void write_zero_input_sol(const vector<double>& y, const string header, const string file_name)
{
  ofstream outfile(file_name);
  outfile << header << endl;
  for (int i = 0; i < y.size(); i++)
  {
    outfile << y[i] << endl;
  }
  outfile << endl;
  outfile.close();
}

/*
    Initializes square matrix.
*/
void init_square_matrix(matrix& A, const int order)
{
  for (int i = 0; i < order; i++)
  {
    vector<double> row(order, 0.0);
    // cout << "row = " << row.size() << endl;
    A.push_back(row);
  }
}
;